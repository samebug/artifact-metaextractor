name := "samebug-metaextractor"
version := "0.0.0-SNAPSHOT"
scalaVersion := "2.11.8"

scalacOptions ++= Seq("-deprecation:true", "-Ylog-classpath:false", "-feature:true")

libraryDependencies ++= Seq(
  "com.github.javaparser" % "javaparser-core" % "2.5.1"
)