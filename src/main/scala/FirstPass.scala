import java.io.FileInputStream

import com.github.javaparser.JavaParser
import com.github.javaparser.ast.CompilationUnit
import com.github.javaparser.ast.body.{BodyDeclaration, ConstructorDeclaration, TypeDeclaration}
import com.github.javaparser.ast.comments.JavadocComment

import scala.collection.JavaConverters._

object FirstPass extends App {
  val is = new FileInputStream("/Users/rp/Documents/Git/artifact-metaextractor/testdata/mysql/com/mysql/jdbc/Driver.java")
  try {
    val cu: CompilationUnit = JavaParser.parse(is);
    val t: TypeDeclaration = cu.getTypes.asScala.head

    val typeComment: JavadocComment = t.getJavaDoc
    val members: Seq[BodyDeclaration] = t.getMembers.asScala
    val constructors: Seq[ConstructorDeclaration] = members filter {_.isInstanceOf[ConstructorDeclaration]} map {_.asInstanceOf[ConstructorDeclaration]}
    val defaultConstructor: ConstructorDeclaration = constructors.head

    val constructorComment: JavadocComment = defaultConstructor.getJavaDoc

    constructorComment.getAllContainedComments.asScala foreach println
  } finally {
    is.close()
  }
}
